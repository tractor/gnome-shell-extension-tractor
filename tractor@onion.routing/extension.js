const Clutter = imports.gi.Clutter;
const ExtensionUtils = imports.misc.extensionUtils;
const Lang = imports.lang;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const St = imports.gi.St;
const Extension = ExtensionUtils.getCurrentExtension();
const Util = imports.misc.util;

const Tractor = new Lang.Class({
  Name: 'Tractor',
  Extends: PanelMenu.Button,	// Parent Class

  // Constructor
  _init: function() {
    /*
       This is calling the parent constructor
       1 is the menu alignment (1 is left, 0 is right, 0.5 is centered)
       `PopupMenuExample` is the name
       true if you want to create a menu automatically, otherwise false
    */
    this.parent(1, 'PopupMenuExample', false);

    // We are creating a box layout with shell toolkit
    let box = new St.BoxLayout();

    /*
      A new icon 'system-search-symbolic'.symbolic
      All icons are found in `/usr/share/icons/theme-being-used`
      In other tutorials we will teach you how to use your own icons

      The class 'system-status-icon` is very useful, remove it and restart the shell then you will see why it is useful here
    */
    //let icon =  new St.Icon({ icon_name: 'system-search-symbolic', style_class: 'system-status-icon'});

    // A label expanded and center aligned in the y-axis
    let toplabel = new St.Label({ text: ' Tractor ',
                                  y_expand: true,
                                  y_align: Clutter.ActorAlign.CENTER });

    // We add the icon, the label and a arrow icon to the box
    //box.add(icon);
    box.add(toplabel);
    box.add(PopupMenu.arrowIcon(St.Side.BOTTOM));

    // We add the box to the button
    // It will be showed in the Top Panel
    this.actor.add_child(box);


    // Other standard menu items
    let startmenuitem = new PopupMenu.PopupMenuItem('Start');
    let stopmenuitem = new PopupMenu.PopupMenuItem('Stop');
    let restartmenuitem = new PopupMenu.PopupMenuItem('Restart');
    let newidmenuitem = new PopupMenu.PopupMenuItem('New ID');
    let setmenuitem = new PopupMenu.PopupMenuItem('Set');
    let unsetmenuitem = new PopupMenu.PopupMenuItem('Unset');
    //let switchmenuitem = new PopupMenu.PopupSwitchMenuItem('PopupSwitchMenuItem');

    // Assemble all menu items
    // This is a menu separator
    this.menu.addMenuItem(startmenuitem);
    this.menu.addMenuItem(stopmenuitem);
    this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
    this.menu.addMenuItem(restartmenuitem);
    this.menu.addMenuItem(newidmenuitem);
    this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
    this.menu.addMenuItem(setmenuitem);
    this.menu.addMenuItem(unsetmenuitem);
    this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

    /*
      With Popup*MenuItem you can use the signal `activate`, it is fired when the user clicks over a menu item
    imagemenuitem.connect('activate', Lang.bind(this, function(){
      toplabel.set_text('Changed');
    }));
    */

    startmenuitem.connect('activate', Lang.bind(this, function(){
      Util.spawnCommandLine("tractor start");
    }));
    stopmenuitem.connect('activate', Lang.bind(this, function(){
      Util.spawnCommandLine("tractor stop");
    }));
    restartmenuitem.connect('activate', Lang.bind(this, function(){
      Util.spawnCommandLine("tractor restart");
    }));
    newidmenuitem.connect('activate', Lang.bind(this, function(){
      Util.spawnCommandLine("tractor newid");
    }));
    setmenuitem.connect('activate', Lang.bind(this, function(){
      Util.spawnCommandLine("tractor set");
    }));
    unsetmenuitem.connect('activate', Lang.bind(this, function(){
      Util.spawnCommandLine("tractor unset");
    }));

  },

  /*
    We destroy the button
  */
  destroy: function() {
    /*
      This call the parent destroy function
    */
    this.parent();
  }
});

/* Global variables for use as button to click */
let button;

/*
  This is the init function, here we have to put our code to initialize our extension.
  we have to be careful with init(), enable() and disable() and do the right things here.
  In this case we will do nothing
*/
function init() {}

/*
  We have to write here our main extension code and the things that actually make works the extension(Add ui elements, signals, etc).
*/
function enable() {
  /* Create a new object button from class PopupMenuExample */
  button = new Tractor;

  /*
     In here we are adding the button in the status area
     - `PopupMenuExample` is tha role, must be unique. You can access it from the Looking Glass  in 'Main.panel.statusArea.PopupMenuExample`
     - button is and instance of panelMenu.Button
     - 0 is the position
     - `right` is the box where we want our button to be displayed (left/center/right)
  */
  Main.panel.addToStatusArea('Tractor', button, 0, 'right');
}

/*
  We have to delete all conections and things from our extensions, to let the system how it is before our extension. So
  We have to unconnect the signals we connect, we have to delete all UI elements we created, etc.
*/
function disable() {
  /*
     We call the destroy function inside the object button
     therefore the button is remove from the panel
  */
  button.destroy();
}
